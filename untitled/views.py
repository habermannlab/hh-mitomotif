from django.http import HttpResponse, Http404
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import redirect, render
from datetime import datetime
from .forms import ContactForm
from django.contrib import messages


def main_home(request):
	return render(request, 'home.html')


def redirect_home(request):
	return redirect('/home/')


def contact(request):
	if request.method == 'GET':
		form = ContactForm()
	else:
		form = ContactForm(request.POST)
		if form.is_valid():
			Subject = form.cleaned_data['Subject']
			Message = form.cleaned_data['Message']
			Sender = form.cleaned_data['Sender']
			try:
				send_mail(Subject, Message, Sender, ['admin@admin.com'])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return redirect('/contactSuccess')
	return render(request, "contact.html", {'form': form})


def successView(request):
	return render(request, 'success.html')


def about(request):
	return render(request, 'about.html')
