# Generated by Django 2.1.7 on 2019-04-15 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HHmitosig', '0003_auto_20190415_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='run',
            name='input_seq',
            field=models.CharField(default='', max_length=500),
        ),
    ]
