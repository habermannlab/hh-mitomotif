from django.urls import path, re_path
from . import views
from .models import ID_length

urlpatterns = [
    path('', views.home, name='HHmitosig_home'),
    re_path(r'^about$', views.view_about, name='about_page'),
    path('mTP/', views.view_mtp, name='mTP_page'),
    re_path(r'^nTP$', views.view_ntp, name='nTP_page'),
    re_path(r'^waiting$', views.view_waiting, name='waiting_page'),
    re_path(r'^redirection$', views.view_redirection, name='redirection_page'),
    # re_path(r'^result$', views.view_output, name='output_page'),
    re_path(r'^(?P<pk>\w{ID_length})/$', views.view_output, name='run_page')
]
