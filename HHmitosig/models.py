import uuid
from django.db import models
from django.utils import timezone
from django.utils.http import int_to_base36
from django.core.exceptions import ValidationError
from Bio import SeqIO

ID_length = 7                                                           # Length of generated IDs


def id_gen() -> str:
    """generate a ID number to be used as Primary key"""
    return int_to_base36(uuid.uuid4().int)[:ID_length]


def is_fasta(filename):
    n = 0
    for record in SeqIO.parse(filename, 'fasta'):
        n += 1
    if n == 0: raise ValidationError("No FASTA sequence detected")
    elif n == 1: return filename
    else: raise ValidationError("There should be only one FASTA sequence per file")


# Create your models here.
class Run(models.Model):
    id = models.CharField(max_length=ID_length,
                          primary_key=True,
                          default=id_gen,
                          editable=False)
    email = models.CharField(max_length=20,
                             default='youpi@gmail.com',
                             null=True)
    input_seq = models.FileField()
    # validators=[is_fasta])
    input_date = models.DateTimeField(default=timezone.now,
                                      verbose_name="input date")
    type = models.BooleanField(default=True)                            # True = mTP, False = nTP
    output = models.CharField(max_length=50,
                              null=True)
    output_date = models.DateTimeField(default=timezone.now,
                                       verbose_name="output date")

    class Meta:
        verbose_name = "run_instance"
        ordering = ['input_date']

    def __str__(self):
        return self.id

    def set_email(self, new_email):
        Run.email = new_email

    def set_type(self, new_type):
        Run.type = new_type

    def start_countdown(self, new_output_date):
        output_date = new_output_date
