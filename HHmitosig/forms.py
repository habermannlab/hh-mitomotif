from django import forms
from .models import Run, id_gen
from Bio import SeqIO
from django.conf import settings
from django.core.files.base import ContentFile
import os
import os.path


class InputForm(forms.Form):
    sequence = forms.CharField(widget=forms.Textarea,
                               help_text='Paste ONE input sequence in FASTA format here',
                               required=True)


class mTPForm(forms.ModelForm):
    class Meta:

        model = Run
        fields = ['input_seq']
        labels = {
            'input_seq': 'Enter one FASTA sequence'
        }

    def clean_input_seq(self):
        input_seq = self.cleaned_data['input_seq']
        return input_seq


class nTPForm(forms.ModelForm):
    class Meta:
        model = Run
        fields = ['input_seq']
        labels = {
            'input_seq': 'Enter one FASTA sequence'
        }

    def clean_input_seq(self):
        input_seq = self.cleaned_data['input_seq']
        return input_seq