from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils import timezone
from django.urls import reverse
from django.contrib import messages
from datetime import datetime
from .forms import InputForm, mTPForm
from .models import Run

# Create your views here.


def view_redirection(request):
	return HttpResponse("You will be redirected soon")


def home(request):
	return render(request, 'home_mitosig.html')


def view_mtp(request):
	sauvegarde = False
	if request.method == "POST":
		form = mTPForm(request.POST or None, request.FILES)
		print('caca')
		if form.is_valid():
			print('TROUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU')
			run_instance = form.save()
			run_instance.input_date = timezone.now()
			print(run_instance.input_date)
			run_instance.type = True
			print(run_instance.type)
			run_instance.save()
			print(run_instance.id)
			url = '{}{}'.format('HHmitosig_home', run_instance.id)
			messages.info(request, 'The model have been saved')
			sauvegarde = True
			return redirect(url, pk=run_instance)
		else:
			print('EFBZVJGRBEUOGRERRBUIN')
	else:
		form = mTPForm()
	return render(request, 'mTP.html', {'form': form})


def view_output(request, pk):
	return render(request, 'output.html')
   

def view_ntp(request):
	form = nTPForm(request.POST or None, instance=Run)
	# form = nTPForm(request.POST or None, instance=run)
	if form.is_valid():
		form.save()
		return redirect('HHmitosig:waiting_page')
	return render(request, 'nTP.html', locals())


def view_about(request):
	return HttpResponse(f'About HH-MitoSig')


def view_waiting(request):
	return render(request, 'waiting.html')

#def list_profiles(request):

#    return render(request, 'navbar.html', {'date': datetime.now})
